package shiq.com.order_module.fragment

import com.alibaba.android.arouter.facade.annotation.Route
import kotlinx.android.synthetic.main.fragment_outstanding_order.*
import shiq.com.common.base.basemvp.MvpFragment
import shiq.com.common.utils.ArouterConst
import shiq.com.order_module.R
import shiq.com.order_module.adapter.OutStandingOrderAdapter
import shiq.com.order_module.bean.OutStandingOrderBean
import shiq.com.order_module.contract.StoreManagerFragContract
import shiq.com.order_module.presenter.StoreManagerFragPresenter

/**
 * created by shi on 2019/6/13/013
 */
@Route(path = ArouterConst.OrderModule.order_storemanagerfrag)
class StoreManagerFragment: MvpFragment<StoreManagerFragPresenter>(),
        StoreManagerFragContract.View {
    override fun setOnClickListener() {

    }

    override fun initView() {

    }

    override fun setResourceId(): Int {
        return R.layout.fragment_store_manager
    }

    override fun createPresenter(): StoreManagerFragPresenter {
        return StoreManagerFragPresenter(this)
    }

    override fun initdata() {

    }


}

