package shiq.com.common.base.basemvp

import shiq.com.common.base.ViewPagerBaseFragment

/**
 * created by shi on 2019/6/17/017
 *
 */
abstract class MvpFragment<P : BasePresenter<*>> : ViewPagerBaseFragment() {

    protected lateinit var mvpPresenter: P

    override fun initPresenter() {
        mvpPresenter = createPresenter()
        lifecycle.addObserver(mvpPresenter)
    }

    abstract fun createPresenter(): P

    override fun onDestroyView() {
        super.onDestroyView()
        if(mvpPresenter!=null){
            mvpPresenter.detachView()
        }
    }


    override fun onVisible() {

    }


}